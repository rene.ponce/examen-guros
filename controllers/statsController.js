const Dna = require('../database/models').Dna;

const getStats = async (req, res) => {
  try {
    const mutation = await Dna.findAndCountAll({
      where:{
        hasMutation: true
      }
    });
    const noMutation = await Dna.findAndCountAll({
      where: {
        hasMutation: false
      }
    });
    res.json({
      count_mutations: mutation.count,
      count_no_mutation: noMutation.count,
      ratio: mutation.count / noMutation.count
    });
  } catch (error) {
    
  }
}

module.exports = {getStats};