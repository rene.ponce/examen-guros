const mutation = require('../lib/mutation.js');
const Dna = require('../database/models').Dna;

const hasMutation = (req, res) => {
  const {dna} = req.body;
  // Check mutation
  const check = mutation.mutation(dna)
  const hasMutation = check.length > 1;

  // Save in data base
  saveDb(dna, hasMutation);

  (hasMutation) ? res.send(hasMutation) : res.status(403).send(hasMutation);
};

const saveDb = async (dna, hasMutationBool) => {
  try {
    Dna.create({
      dna: dna.join(','),
      hasMutation: hasMutationBool
    });
  } catch (error) {
    return error;
  }
};

module.exports = {hasMutation};