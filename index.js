const express = require('express');
const dotenv = require('dotenv');

dotenv.config();

const app = express();
const PORT = process.env.PORT || 8080;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Routes
app.use('/mutation', require('./routes/mutation'));
app.use('/stats', require('./routes/stats'));

// Start server
app.listen(PORT, () => console.log(`Server is running on PORT ${PORT}`));
