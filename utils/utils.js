const getRightDiagonal = (m) => {
	let s, x, y, d, o = [];
	for (s = 0; s < m.length; s++) {
	  d = [];
	  for(y = s, x = 0; y >= 0; y--, x++)
		d.push(m[y][x]);
	  o.push(d);
	}
	for (s = 1; s < m[0].length; s++) {
	  d = [];
	  for(y = m.length - 1, x = s; x < m[0].length; y--, x++)
		d.push(m[y][x]);
	  o.push(d);
	}
	return o.map((array) => {
    return array.join('');
  });
}

const getLeftDiagonal = (m) => {
    let reverse = reverseMatrix(m);
    return getRightDiagonal(reverse);
};

const reverseString = (string) => {
    return string.split("").reverse().join("");
};

const reverseMatrix = (m) => {
    return m.map((string) => {
      return reverseString(string);
    });
};

module.exports = {
  getRightDiagonal,
  getLeftDiagonal,
  reverseString,
  reverseMatrix
}