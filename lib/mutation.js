const utils = require('../utils/utils.js');

const mutation = (dnaArray) => {
  const regex = /([ATGC])\1{3,4}/;

  const line = dnaArray.filter((str) => {
    return regex.test(str);
  })
  
  const right = utils.getRightDiagonal(dnaArray).filter((str) => {
    return regex.test(str);
  });

  const left = utils.getLeftDiagonal(dnaArray).filter((str) => {
    return regex.test(str);
  });
  return line.concat(right).concat(left);
};

module.exports = {mutation};