const express = require('express');
const ctrl = require('../controllers/mutationController');


const router = express.Router();

router.post('/', ctrl.hasMutation);

module.exports = router;
