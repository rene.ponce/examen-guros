const express = require('express');
const ctrl = require('../controllers/statsController');


const router = express.Router();

router.get('/', ctrl.getStats);

module.exports = router;