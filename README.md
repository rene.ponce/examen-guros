# Instrucciones

Examen Guros

## Api

Entrar al directorio backend-server que se encuentra dentro del directorio Biblioteca y ejecutar el siguiente comando

```bash
npm install
```

Renombrar el archivo .env.example por .env

El proyecto de backend-server utiliza docker para tener un contenedor con PostgreSQL, una vez instaladas las dependencias hay que levantar el contenedor de la base de datos mediante docker compose con el siguiente comando

```bash
docker-compose up --build
```

Una vez terminado de instalar las dependencias y que se tiene arriba el contenedor de docker hay que ejecutar el siguiente comando para crear las tablas

```bash
npm run migrate
```

Para levantar el servidor se utiliza el siguiente comando

```bash
npm run start:dev
```

## Tecnologías utilizadas

- NodeJS LTS
- Docker
- Docker compose
- Sequelize
